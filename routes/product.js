const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");

// Route for creating a Product
router.post("/", (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(resultFromController => res.send (
		resultFromController));
});

module.exports = router;

// Retrieve all products
router.get("/active", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
})

// Retrieve specific product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})

// Update Product
router.put("/:productId", auth.verify, (req, res) => {
	let data = {
		product : req.body,
		productId : req.params.productId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	productController.updateProduct(data).then(resultFromController => res.send(resultFromController));
})

// Archive Product
router.put("/:productId/archive", auth.verify, (req, res) =>{

    const data = {
        product : req.body,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }
    productController.archiveProduct(req.params, data).then(resultFromController => res.send(resultFromController))
})

module.exports = router;