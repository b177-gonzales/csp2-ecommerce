const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

// Route for registering a user.
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for authenticating user
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})

// Route for setAsAdmin
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	const data = {
		userId : req.params.userId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	userController.setAsAdmin(data).then(resultFromController => res.send(resultFromController))
})

// Retrieve all users
router.get("/all", (req, res) => {
	userController.getAllUsers().then(resultFromController => res.send(resultFromController));
})

// Create Order
router.post("/checkout", auth.verify, (req, res) => {

	 const data = {
        order: req.body,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }

	console.log(data);

	userController.createOrder(data).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all orders
router.get("/orders", auth.verify, (req, res) => {
    const isAdmin = auth.decode(req.headers.authorization).isAdmin;
    userController.getAllOrders(isAdmin).then(resultFromController => res.send(resultFromController));
})

// Route for retrieving a specific user's orders
router.get("/:userId/myorder", auth.verify, (req, res) => {
    const userId = auth.decode(req.headers.authorization).id;
    userController.getMyOrder(userId).then(resultFromController => res.send(resultFromController));
})

module.exports = router;
