const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Controller function for user registration
module.exports.registerUser = (reqBody) => {
	// Creates variable "newUser" and instantiates a new "User" object using the mongoose model
	let newUser = new User({
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	// Saves the created object to our database
	return newUser.save().then((user, error) => {
		// User registration failed
		if(error){
			return false;
		}
		// User registration successful
		else{
			return true;
		}
	})
}

// User authentication (/login)
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		// User does not exist
		if(result == null){
			return false;
		}
		// User exists
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				// Generate an access token
				return { access : auth.createAccessToken(result)}
			}
			// Passwords do not match
			else{
				return false;
			}
		}
	})
}

module.exports.setAsAdmin = (data) => {
	if (data.isAdmin) {
		let updateAdmin = {
			isAdmin : true
		};

		return User.findByIdAndUpdate(data.userId, updateAdmin).then((user, error) => {
			if (error) {
				return false;
			}
			else {
				return true;
			}
		})
	}
	else {
		return Promise.resolve(false);
	}
};

// Retrieve all users
module.exports.getAllUsers = () => {
		return User.find({isActive: true}).then(result => {
			return result;
		})
}

// Create Order
module.exports.createOrder = (data) => {

		let newOrder = new Order({
			userId : data.order.userId,
			productId : data.order.productId,
			totalAmount : data.order.totalAmount
		});

		// Saves the created object to our database
		return newOrder.save().then((order, error) => {

			// Order creation failed
			if (error) {

				return false;

			// Order creation successful
			} else {

				return true;

			}

		})
}

// Retrieve all orders (/orders)
module.exports.getAllOrders = (isAdmin) => {
    if (isAdmin) {
        return Order.find({}).then((orders, error) => {
            if (error) {
                return false;
            }
            else {
                return orders;
            }
        }) 
    }
    else {
        return Promise.resolve(false);
    }
}

// Retrieve a specific user's orders
module.exports.getMyOrder = (userId) => {
    if (userId) {
        return Order.find({'user': userId}).then((orders, error) => {
            if (error) {
                return false;
            }
            else {
                return orders;
            }
        }) 
    }
    else {
        return Promise.resolve(false);
    }

}

