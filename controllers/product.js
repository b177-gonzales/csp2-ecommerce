const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.addProduct = (data) => {

	// User is an admin
	if (data.isAdmin) {

		// Uses the information from the request body to provide all the necessary information
		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		});

		// Saves the created object to our database
		return newProduct.save().then((product, error) => {

			// Product creation failed
			if (error) {

				return false;

			// Product creation successful
			} else {

				return true;

			};

		});

	// User is not an admin
	} else {
		return Promise.resolve(false);
	};
	

};

// Retrieve all products
module.exports.getAllProducts = () => {
		return Product.find({isActive: true}).then(result => {
			return result;
		})
}


// Retrieve specific product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

// Update Product
module.exports.updateProduct = (data) => {
	if(data.isAdmin) {
		let updatedProduct = {
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		}
		return Product.findByIdAndUpdate(data.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return Promise.resolve(`Product updated.`);
			}
		})
	}
}

//Archive Product
module.exports.archiveProduct = (reqParams, data) => {

    if(data.isAdmin) {

        let archivedProduct = {

            isActive : false
        }

        return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((product, error) => {
            
            if (error) {
                return false;
            }
            else {
                return true;
            }
        })
    }
    // Non-admin cannot archive a product
    else {
        return Promise.resolve(false);
    }
}
